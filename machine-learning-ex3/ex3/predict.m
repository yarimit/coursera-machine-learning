function p = predict(Theta1, Theta2, X)
%PREDICT Predict the label of an input given a trained neural network
%   p = PREDICT(Theta1, Theta2, X) outputs the predicted label of X given the
%   trained weights of a neural network (Theta1, Theta2)

% Useful values
m = size(X, 1);
num_labels = size(Theta2, 1);

% You need to return the following variables correctly 
p = zeros(size(X, 1), 1);

% ====================== YOUR CODE HERE ======================
% Instructions: Complete the following code to make predictions using
%               your learned neural network. You should set p to a 
%               vector containing labels between 1 to num_labels.
%
% Hint: The max function might come in useful. In particular, the max
%       function can also return the index of the max element, for more
%       information see 'help max'. If your examples are in rows, then, you
%       can use max(A, [], 2) to obtain the max for each row.
%

% Theta1 [25 x 401]
% Theta2 [10 x 26]

X401 = [ones(m,1) X];       % [5000 x 401] Xの先頭に1の列追加

for n=1:m
    a1 = X401(n, :);            % [1 x 401]
    a2_1 = a1 * Theta1';        % [1 x 25]
    a2_2 = sigmoid(a2_1);       % [1 x 25]
    a2 = [1 a2_2];              % [1 x 26]

    a3 = sigmoid(a2 * Theta2');   % [1 x 10]

    % 一番でかいのを取る
    [v, idx] = max(a3);
    p(n) = idx;
end

% =========================================================================


end
