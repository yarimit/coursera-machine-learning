function [J, grad] = costFunctionReg(theta, X, y, lambda)
%COSTFUNCTIONREG Compute cost and gradient for logistic regression with regularization
%   J = COSTFUNCTIONREG(theta, X, y, lambda) computes the cost of using
%   theta as the parameter for regularized logistic regression and the
%   gradient of the cost w.r.t. to the parameters. 

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta.
%               You should set J to the cost.
%               Compute the partial derivatives and set grad to the partial
%               derivatives of the cost w.r.t. each parameter in theta

% theta [28 x 1]
hx = sigmoid(X * theta);
p = (lambda / (2 * m)) * (theta' * theta);
J = (1/m) * (-y' * log(hx) - (1 - y)' * log(1 - hx)) + p;
p2 = lambda / m * theta; % 定数項 [1 x 28]
p2(1) = 0;               % grad(1) の定数項のみ0
temp_grad = (1/m) * ((hx - y)' * X) + p2';   % p2を転置して足す
grad = temp_grad';   % gradは[1 x 28] じゃなくて [28 x 1] の形にならないとsubmitが通らないらしい

% ネットから拾ってきたネタバレコード
%h = sigmoid(X*theta);
%theta1 = [0 ; theta(2:size(theta), :)];
%p = lambda*(theta1'*theta1)/(2*m);
%J = ((-y)'*log(h) - (1-y)'*log(1-h))/m + p;
%grad = (X'*(h - y)+lambda*theta1)/m;

% =============================================================

end
